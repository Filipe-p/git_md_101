# Markdown and Git Basics

Markdown and git / Github / Birbucket are extremely useful tools to document your work. 

We will use them a lot. 

### Previously today
- git
- bash basics 
- connecting ssh to bitbucket 

### In this document
- markdown basics 

### What is Markdown
It is a mark up language that allows you to use simple syntax to then create visial changes. It differs from a programmable language where you cannot set variables, have logical operation. Rather you just set tags to content. 

### Syntax

To make header 
```
Title #
Subtitle ##
Header 3 ###
and so on

```

To make bullet points and number
```
for bullet points
- bullet
- bullet 

for numbered points
1. one 
2. two points

```

To make `code snipits` 
```
inline code `code`

Code block with out the " 
  "```"
  "```"
```

How to attach an image: 


How to make references (links to other sections)


How to make things BOLD


How to make italic 


How to add Emojis

